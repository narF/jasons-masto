# Jasons!
Assied toi et jasons un peu!

Un bot pour susciter des bonnes conversations, que ce soit entres amis, entres résidents du fédiverse ou par un dialogue intérieur avec soi-même.

### Pour installer
1. `git clone git@codeberg.org:narF/jasons-masto.git`
2. `cd jasons-masto.git`
3. `pip3 install Mastodon.py`

### Pour configurer
1. Obtenir un token du serveur masto
2. Créer un fichier token.secret et coller le token dedans. Pas de guillemets, pas de sauts de ligne. Juste le token.

## Crontab
Faire ceci pour rouler le bot à chaque 6h:

1. `crontab -e`
2. Ajouter ceci:
```
#à chaque 6h, post un toot de jasons. Écrit le log et les erreurs dans /tmp
31 */6 * * *	cd /path/to/jasons-masto && python3 main.py > /tmp/jasons-prod.log 2>&1
```


### Pour exécuter manuellement

`python3 main.py`

