from mastodon import Mastodon

masto = None

def startMasto(url):
	print("Mastodon URL:", url)
	
	global masto
	
	# Set up Mastodon
	masto = Mastodon(access_token = 'token.secret', api_base_url = url)
	
	
def post(text):
	masto.status_post(text)
