import json
import random


# Importe le fichier json et le load dans "questions"
with open("questions.json", mode='r', encoding='utf-8') as q:
    questions = json.load(q)
    questions = questions["questions"] # get rid of unecessary data (colors, text strings)



def pigeInAny():
    all = questions["1"] + questions["2"] + questions["3"]
    q = random.sample( all, 1) # pige 1 dans all
    return q[0] #pour le sortir du format List

def pigeIn(category):
    return random.sample( questions[category], 1 )[0]


# toto = random.random()
